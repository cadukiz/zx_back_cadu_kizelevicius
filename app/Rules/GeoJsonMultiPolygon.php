<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class GeoJsonMultiPolygon implements Rule
{


    private $validProperties = ['type', 'coordinates'];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       // dd($value);die;
       if (!is_array($value)){
        $array = get_object_vars(json_decode($value));

       }else{
           $array = $value;
       }
        
       
        if (is_array($array) === false) {
            return false;
        }

       
            $properties = $array;

            if (count($properties) !== 2) {
                return false;
            }

            $propertyNames = array_keys($properties);
            //print count(array_diff($this->validProperties, $propertyNames));die;
            if (count(array_diff($this->validProperties, $propertyNames)) > 0) {
                return false;
            }
            //print $properties['type']; die;

            if ($properties['type']!='MultiPolygon'){
                return false;
            }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Must be a GeoJson MultiPolygon.';
    }
}
