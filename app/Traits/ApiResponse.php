<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use function request;
use function response;
use function str_replace;
use function strpos;
use function strtotime;
use App\Http\Resources\CompaniesResource;

trait ApiResponse
{
    protected function showAll($response,$code=200,$pluck=null,$get=false)
    {
       
        //$response = $collection;
        //dd($response);


        //return $response->paginate(15);

        //dd($response);


        $response = $this->_filter($response);
        $response = $this->_between($response);
        $response = $this->_fields($response);
        $response = $this->_embed($response);
        $response = $this->_count($response);

        if(request()->sort_by){ // priorize sort_by from request 
            $response->getQuery()->orders = null;
        }


        $response = $this->_sort($response);

        //$response = $this->_pluck($response,$pluck);
        
       // $response = $response->paginate(15);

       // dd($reponse);

        // $response = $this->_paginate($response);// Apply Paginate

       // dd($response);
        //return $response->get();


        // $data = ['data'=>$response['content']];
        $data = ['data'=>$response->get()];

    //  return response()->json($data,$code)->withHeaders($response['header']);
     return response()->json($data,$code);
    }

    protected function showOne($model,$code=200)
    {

        if(@$model->id) {
        $response = $model;
        $response = $this->_fields($response);
        $response = $this->_embed($response);
        
       
            $response = $response->find($model->id);

            $data = ['data'=>$response];
            return response()->json($data,$code);
        }

        return response()->json(null,$code);
    }

    protected function showResponse($data,$code=200)
    {
       
       // return $model->paginate(15);
       $data = ['data'=>$data];

        return response()->json($data,$code);
        
    }


    protected function showDeleted($model)
    {
        return response()->json($model);
    }

    private function _pluck($response,$pluck)
    {
        if($pluck)
        {
            $response = $response->pluck($pluck)
                ->collapse();
        }
        return $response;
    }

    private function _sort($response)
    {

       // dd(request()->sort_by);

        if(request()->sort_by)
        {
            $attribute = request()->sort_by;
            if(strpos($attribute,'-') !== false)
            {
                $attribute = str_replace('-','',$attribute);
                return $response->orderBy($attribute, 'DESC');
            }
            return $response->orderBy($attribute,'ASC');
        }
        return $response;
    }

    private function _fields($response)
    {
        if(request()->fields)
        {
            $fields = $this->getArray(request()->fields);
            return $response->select($fields);
        }
        return $response;
    }
    private function _embed($response)
    {
        if(request()->embed)
        {
            $embed = $this->getArray(request()->embed);
            return $response->with($embed);
        }
        return $response;
    }

    private function _filter($response)
    {
       // dd(request()->query());

        $actions = ['sort_by','embed','pluck','between','count','per_page','pages','page'];

        foreach (request()->query() as $attribute => $value){

             //   dd($attribute)

                    if (!in_array($attribute,$actions)){
                      // dd($attribute);
                        if ($attribute == 'tags'){
                            $response->where($attribute,'LIKE','%'.$value.'%');
                        }else{
                            $response->where($attribute,$value);
                        }
                        
                    } 
                    
                
            
        }

        return $response;
    }




    private function _between($response)
    {
        if(request()->between)
        {
            $between = $this->getArray(request()->between);
            if(count($between) !== 3){
                return $response;
            }
            $column         = $between[0];
            $start_value    = $between[1];
            $end_value      = $between[2];

            return $response->whereDate($column,'>=', $start_value)
                ->whereDate($column,'<=',$end_value);
        }
        return $response;
    }
    private function _count($response){
        if(request()->count_related){
            $response  = $response->withCount(request()->count_related);
        }
        return $response;
    }

  
    private function getArray($fields)
    {
        if($fields)
        {
            return explode(',',$fields);
        }
        return null;
    }

    protected function success($message, $code=200, $debug=''){
        $data =[
            'message'=>$message,
            'error'=>null,
            'code'=>$code,
            'debug'=>$debug
        ];
        $data = ['data'=>$data];
        return response()->json($data, $code);
    }

    protected function error($errors=[], $message='', $code=400,$debug=''){


        $data =[
            'message'=>$message,
            'error'=>$errors,
            'code'=>$code,
            'debug'=>$debug
        ];

        //dd($_ENV);//?$data['debug']=$debug:null ;
        $data = ['data'=>$data];
        return response()->json($data, $code);
    }

    protected function showMessage($message, $code=200){
        return response()->json($message,$code);
    }

    private function _paginate($response){

       $perPage = request()->per_page ?: 15;

       $paginated = $response->paginate($perPage);

      // dd($paginated);

        //return $this;




            $paginated->appends(request()->all());   
            $pages = $paginated->toArray();
            return [
                'content'=>$paginated->items(),
                'header'=>[
                    'X-Pagination-first_page_url'=>$pages['first_page_url'],
                    'X-Pagination-next_page_url'=>$pages['next_page_url'],
                    'X-Pagination-prev_page_url'=>$pages['prev_page_url'],
                    'X-Pagination-last_page_url'=>$pages['last_page_url'],
                    //'Links'=>$this->_formatLinksToHeader($paginated),
                    'X-Pagination-Total'=>$paginated->total(),
                    'X-Pagination-PerPage'=>$paginated->perPage(),
                    'X-Pagination-LastPage'=>$paginated->lastPage(),
                    'X-Pagination-CurrentPage'=>$paginated->currentPage(),
                ]
            ];



    }

   // to Paginate ArrayCollections

    protected function showPaginated($response){

        //dd($response);
        if (!is_array($response)){
          $response =  $response->toArray();
        }
        

        $page = LengthAwarePaginator::resolveCurrentPage();

        //dd($page);
        $perPage = request()->per_page ?: 15;
        $results = array_slice($response, ($page - 1) *$perPage, $perPage);

           // dd($results);
        $paginated = new LengthAwarePaginator($results, count($response), $perPage, $page, [
            'path'=>LengthAwarePaginator::resolveCurrentPath()
        ]);
        $paginated->appends(request()->all());  
       // if (!is_array($pages)) {
            $pages = $paginated->toArray();
        //}
        $return =  [
            'content'=>$paginated->items(),
            'header'=>[
                'X-Pagination-first_page_url'=>$pages['first_page_url'],
                'X-Pagination-next_page_url'=>$pages['next_page_url'],
                'X-Pagination-prev_page_url'=>$pages['prev_page_url'],
                'X-Pagination-last_page_url'=>$pages['last_page_url'],
                //'Links'=>$this->_formatLinksToHeader($paginated),
                'X-Pagination-Total'=>$paginated->total(),
                'X-Pagination-PerPage'=>$paginated->perPage(),
                'X-Pagination-LastPage'=>$paginated->lastPage(),
                'X-Pagination-CurrentPage'=>$paginated->currentPage(),
            ]
        ];

           // dd($response);
        
         return response()->json($return['content'],200)
             ->withHeaders($return['header']);
}

    private function _formatLinksToHeader($paginated)
    {
        $pages = $paginated->toArray();
        $first = "<{$pages['first_page_url']}>; rel='first_page_url'";
        $next = "<{$pages['next_page_url']}>; rel='next_page_url'";
        $prev = "<{$pages['prev_page_url']}>; rel='prev_page_url'";
        $last = "<{$pages['last_page_url']}>; rel='last_page_url'";

        $links = "{$first}, {$next}, {$prev}, {$last}";
        return $links;
    }
}