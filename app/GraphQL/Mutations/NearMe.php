<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\Pdv;
use App\Services\Pdv\PdvServices;
use App\Repositories\Pdv\PdvRepository;

class NearMe
{

protected $service;

    public function __construct(PdvServices $service)
    {
        $this->service = $service;
    }

    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        //dd( Pdv::all());
       //// $service as PdvServices;
      //return [new Pdv()];
      //dd($this->service->nearMe(-38.5367295,-3.7953807)->get());
        return $this->service->nearMe($args['lng'],$args['lat'])->get();
    }
}
