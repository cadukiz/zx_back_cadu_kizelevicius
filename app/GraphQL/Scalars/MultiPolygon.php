<?php

namespace App\GraphQL\Scalars;

use GraphQL\Type\Definition\ScalarType;

/**
 * Read more about scalars here http://webonyx.github.io/graphql-php/type-system/scalar-types/
 */

class MultiPolygon extends ScalarType
{
    /**
     * The description that is used for schema introspection.
     *
     * @var string
     */
    public $description = 'Arbitrary data encoded in JavaScript Object Notation. See https://www.json.org/.';

    /**
     * Serializes an internal value to include in a response.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function serialize($value)
    {
        return $value;
    }

    /**
     * Parses an externally provided value (query variable) to use as an input.
     *
     * In the case of an invalid value this method must throw an Exception
     *
     * @param mixed $value
     *
     * @throws Error
     *
     * @return mixed
     */
    public function parseValue($value)
    {
        //dd($value);
        return json_decode($value);
    }

    /**
     * Parses an externally provided literal value (hardcoded in GraphQL query) to use as an input.
     *
     * In the case of an invalid node or value this method must throw an Exception
     *
     * @param Node $valueNode
     * @param mixed[]|null $variables
     *
     * @throws Exception
     *
     * @return mixed
     */
    public function parseLiteral($valueNode, ?array $variables = null)
    {
        

        return $valueNode->value;
    }

    /**
     * Try to decode a user-given value into JSON.
     *
     * @param mixed $value
     *
     * @throws Error
     *
     * @return mixed
     */
   
}
