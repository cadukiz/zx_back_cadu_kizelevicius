<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;


class Pdv extends Moloquent
{
    protected $fillable=[
        'id',
        'tradingName',
        'ownerName',
        'document',
        'coverageArea',
        'address',
    ];

    protected $hidden=[
        '_id'
    ];
}
