<?php

namespace App\Services\Pdv;

use App\Services\BaseService;
use App\Models\Pdv;
use App\Repositories\Pdv\PdvRepository;

class PdvServices extends BaseService
{
    public function __construct(Pdv $model, PdvRepository $repo)
    {
        $this->model = $model;
        $this->repository = $repo;
    }

    public function nearMe($lng,$lat){
        return $this->repository->nearMe($lng,$lat);
    }

    public function getById($id){
       return $this->repository->getById($id);
    }
}