<?php

namespace App\Services;


use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BaseService
{
   
    public $model;
    public $repository;

    /**
     * Store a newly created resource in storage.
     *
     * @param  null
     * @return Jenssegers\Mongodb\Eloquent\Model 
     */

    public function index()
    {
        $list = $this->repository->index();

        return $list;
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  Array  $request
     * @return Jenssegers\Mongodb\Eloquent\Model 
     */
    public function store($data)
    {
        if (!is_array($data['coverageArea'])){ // TODO : need refactor
           $data['coverageArea'] = json_decode($data['coverageArea'],true);
        }
        if (!is_array($data['address'])){
            $data['address'] = json_decode($data['address'],true);
         }

        //dd($data);
        $model = $this->repository->store($data);
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param   $oid
     * @return Jenssegers\Mongodb\Eloquent\Model 
     */
    public function show($oid)
    {
        return $this->repository->show($oid);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = $this->repository->update($request->all(),$id);
        return $model;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
       }


}