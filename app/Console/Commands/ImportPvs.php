<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Pdv;

class ImportPdvs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pdvs:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Pdvs from Json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Now we\'ll import Pdvs');
        $pdv = new Pdv();
        $pdv->tradingName ='imported';
        $pdv->save();
    }
}
