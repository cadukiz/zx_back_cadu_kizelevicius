<?php

namespace App\Helpers;

class GeoTools{

    public static function isValidPosition($long,$lat){
        
        if (self::isValidLatitude($lat) && self::isValidLongitude($long)){
            return true;
        }
        return false;
    }

    private static function isValidLatitude($latitude){
        if (($latitude <= 90)&& ($latitude >= -90)) {
            return true;
        } else {
            return false;
        }
      }

   private static function isValidLongitude($longitude){
    if (($longitude <= 180)&& ($longitude >= -180)) {
           return true;
        } else {
           return false;
        }
    }

}