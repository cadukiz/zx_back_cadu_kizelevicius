<?php

namespace App\Helpers;

class DataFormatter{

    public static function onlyNumbers($string){
        return preg_replace( '/[^0-9]/', '', $string );
    }

}