<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function signup(Request $request)
    { 
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed|case_diff|numbers|letters|symbols',
            'c_password' => ''
        ]);

        $user = new User([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
 
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'    => 'required',
            'password' => 'required|string'
        ]);

        $credentials = $request->all();

        if(!Auth::attempt(['password'=>$credentials['password'], 'email' => $credentials['email']])){
            return response()->json([
                'status'  => 'false',
                'message' => 'Unauthorized'
            ], 200);
        }

        $user        = \auth()->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token       = $tokenResult->token;

        $token->save();

        return response()->json([
            'status'       => 'true',
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'user'         => $user
        ]);
    }

    public function logout(Request $request)
    {
        DB::table('oauth_access_tokens')
            ->where('user_id', $request['user_id'])
            ->update([
                'revoked' => true
        ]);

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
