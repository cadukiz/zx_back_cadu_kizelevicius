<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pdv;
use App\Http\Requests\Pdv\StorePdvRequest;
use App\Services\Pdv\PdvServices;
use App\Helpers\GeoTools;

class PdvController extends Controller
{

    private $service;

    public function __construct(PdvServices $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->showAll($this->service->index());
    }

    public function store(StorePdvRequest $request) // Request validation into StorePdvRequest
    {
       
        return $this->service->store($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pdv  $pdv
     * @return \Illuminate\Http\Response
     */
    public function show($pdv)
    {
        
       $pdv = $this->service->getById($pdv);
       return $this->showResponse($pdv);
    }

    public function nearMe($lng, $lat){

        if (!GeoTools::isValidPosition($lng,$lat)){
            return $this->error(['Invalid Position:'.$lng.','.$lat]);
        };

        $pdvs = $this->service->nearMe($lng,$lat);
        return $this->showAll($pdvs);
    }
}
