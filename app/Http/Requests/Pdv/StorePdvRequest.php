<?php

namespace App\Http\Requests\Pdv;

use App\Http\Requests\ApiFormRequest;
use App\Rules\GeoJsonMultiPolygon;
use App\Rules\GeoJsonPoint;
use App\Helpers\DataFormatter;

class StorePdvRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required|unique:pdvs',
            'tradingName'=>'required',
            'ownerName'=>'required',
            'document'=>'required|unique:pdvs',
            'coverageArea'=>['required', new GeoJsonMultiPolygon],
            'address'=>['required',new GeoJsonPoint],
            //sss
        ];
    }

    /**
     * Get the custom messages for validation errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'tradingName.required' => 'TradingName is required!'
        ];
    }

    protected function getValidatorInstance()
    {
        $data = $this->all();
        $data['document'] = DataFormatter::onlyNumbers($data['document']);
       
        $this->getInputSource()->replace($data);
       // $data = $this->all();
        //print_r($data);die;
        /*modify data before send to validator*/

        return parent::getValidatorInstance();
    }
    
}
