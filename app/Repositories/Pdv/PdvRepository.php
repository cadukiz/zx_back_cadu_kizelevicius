<?php

namespace App\Repositories\Pdv;

use App\Repositories\BaseRepository;
use App\Models\Pdv;

class PdvRepository extends BaseRepository
{
    public function __construct(Pdv $model)
    {
        $this->model = $model;
    }

    public function nearMe($lng,$lat){
        $pdvs = $this->model::where('coverageArea', 'geoIntersects', [
            '$geometry' => [
                'type' => 'Point',
                'coordinates' =>
                    [
                        (float)$lng,
                        (float)$lat,
                    ]
                ,
            ],
        ]);
        return $pdvs;
    }

    public function getById($id){
       return  $this->model->where('id',$id)->first();
    }
}