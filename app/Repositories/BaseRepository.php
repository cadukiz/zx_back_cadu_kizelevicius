<?php

namespace App\Repositories;


use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BaseRepository
{
   
    public $model;

    /**
     * Store a newly created resource in storage.
     *
     * @param  null
     * @return Jenssegers\Mongodb\Eloquent\Model 
     */

    public function index()
    {
        $list = $this->model->query();

        return $list;
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  array
     * @return Jenssegers\Mongodb\Eloquent\Model 
     */
    public function store($data)
    {
        //dd($data);
            $model = $this->model::create($data);
      //dd($model);
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param   $oid
     * @return Jenssegers\Mongodb\Eloquent\Model 
     */
    public function show($oid)
    {
        return $this->model::find($oid);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  array
     * @param  int  $id
     * @return Model
     */
    public function update($data, $id)
    {
        $model = $this->model::where('id',$id);
        $model->fill($data);
        $model->save();
        return $model;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}