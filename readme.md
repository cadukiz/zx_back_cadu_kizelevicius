# ZX Backend Test

Api to manage and find PDVs (drink sellers).
Autentication is implemented but is disable.
You can use RESTful or GraphQL to this API.

    - Dockerized enviroment with:
        - Laravel Framework
        - MongoDB
        - RESTful
        - GraphQL
        - Phpunit

### Rules
Defined by : https://github.com/ZXVentures/code-challenge/blob/master/backend.md

### Tasks (Trello)
https://trello.com/b/ERIc9b9X/zx-backend-test

### Requirements 
 - Git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 - Docker (https://docs.docker.com/install/)
 - Docker-compose (https://docs.docker.com/compose/install/)

## Instalation (Dev / Deploy)
`To avoid conflits with running services, default ports have been changed`

| Service | From | To 
| --- | --- | --- 
| Nginx | 80 | 803 
| MongoDB | 27017 | 27271 

- Install requirements (Above)
- Create a project folder
- On project folder clone: 
```sh
$ git clone https://cadukiz@bitbucket.org/cadukiz/zx_back_cadu_kizelevicius.git zx_ventures
$ git clone https://cadukiz@bitbucket.org/cadukiz/zx_docker_cadu_kizelevicius.git laradock
```
- to Up our services, go to laradock folder with this command:
```sh
$ cd laradock
```
- This step is `ONLY FOR WINDOWS`:  into laradock/.env file edit COMPOSE_PATH_SEPARATOR variable:
    COMPOSE_PATH_SEPARATOR=: 
    to: 
    COMPOSE_PATH_SEPARATOR=;
    .
    .
- Run docker services (Go get a cup of coffee while download)
```sh
$ docker-compose up -d mongo nginx
```
- Log into docker workspace 
```sh
$ docker-compose exec workspace bash
```
- Execute this commands to setup projects
```sh
$ cd zx_ventures
$ cp env-example .env
$ composer install
$ php artisan migrate --seed
$ php artisan passport:install
``` 
- Exit workspace docker container:
```sh
$ exit
``` 
- Add local host to hosts file:
    - On WINDOWS:
         - From Notepad, open the following file: c:\Windows\System32\Drivers\etc\hosts
         - Add this line on the end of file and save: 
            127.0.0.1 api.ze.com.zx
    - Mac / Linux:
        - execute these command:
        ```sh
         $ sudo nano /etc/hosts
        ```
        - add this text:
        127.0.0.1  api.ze.com.zx
        
        -Save File (Ctrl+X....... Y..... Enter)
- `OPTIONAL:` To customize settings (use remote database, change domain, ports, etc), use these files:
    - NGINX: laradock/nginx/sites/api.ze.com.la.conf
    - PROJECT .ENV : zx_ventures/.env
    - LARADOCK .ENV : laradock/.env


# Running Application

##### Local HOST:
http://api.ze.com.zx:803

##### Online Demo HOST:
http://159.65.224.194:803

# Endpoints 
### REST
- Login : {HOST}/api/login (POST)  (email: admin@admin.com.br  password: 123456  to get token.)
- Add PDV:   {HOST}/api/pdvs (POST)   (Include a new PDV)    
- Get PDV By id: {HOST}/api/pdvs/{id} (GET) (Get a specific PDV by id)
- Near Me : {HOST}/api/pdvs/near-me/{longitude},{latitude}  (GET)
  - Using Longitude and latitude search available PDVs (Check if position is on PDV coverage area)).
  - Coods to test: -38.5367295,-3.7953807

### GraphQL
- Connection: {HOST}/graphql 
- Query Interface: {HOST}/graphql-playground
- Get PDV by id: 
    query{ pdv(id:1){  id tradingName ownerName document address coverageArea }}
- Near Me : 
mutation { nearMe (lng:"-38.5367295" lat:"-3.7953807"){ id tradingName ownerName document address coverageArea }}


- Add a PDV (use the entire command):
mutation { addPdv (
id: 74221 tradingName: "Adega do Joao",
ownerName: "Pele Maradona",
document: "96036150644123233212222222s",
address: "{\"type\": \"Point\",\"coordinates\": [-44.012478,-19.887215]}"
coverageArea: "{\"type\": \"MultiPolygon\", \"coordinates\": [[[[-44.04912, -19.87743], [-44.0493, -19.89438], [-44.04758, -19.90212], [-44.04346, -19.90922], [-44.03385, -19.91923], [-44.01891, -19.92165], [-44.01647, -19.92306], [-44.01436, -19.92319], [-44.01175, -19.92427], [-44.00724, -19.92585], [-43.99909, -19.9185], [-43.99432, -19.91403], [-43.99557, -19.90842], [-43.99582, -19.90285], [-43.99436, -19.89002], [-43.99316, -19.8792], [-43.99436, -19.87371], [-43.99951, -19.86532], [-44.01917, -19.85135], [-44.02801, -19.8545], [-44.03745, -19.85668], [-44.04397, -19.8608], [-44.04912, -19.87743]]]]}"      
){id tradingName ownerName document address coverageArea}}