<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Pdv;
use App\Helpers\DataFormatter;

class PdvsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/pdvs.json');
        $data = json_decode($json);
        $i=0;$str="";
        foreach ($data->pdvs as $pdv){

            $obj = new Pdv((array)$pdv);
           
            try{
                $obj->document = DataFormatter::onlyNumbers($obj->document);
                $obj->save();
            }catch (Exception $e){
                $this->command->alert("WARNING: Pdv id:".$obj->id." '".$obj->tradingName."' with overlayed geo points");
            }
            
        }
        //print $str;
    }
}
